import { PageButtonPrevious, PageButtonNext, DivPages } from "./style";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useDispatch } from "react-redux";
import {
  getPokemonThunk,
  getRickAndMortyThunk,
} from "../../store/modules/Characters/thunk";
import { useEffect, useState } from "react";

const ChangePage = () => {
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const nextPage = () => {
    setPage(page + 1);
  };

  const previousPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  useEffect(() => {
    dispatch(getPokemonThunk(page));
    dispatch(getRickAndMortyThunk(page));
  }, [page]);

  return (
    <DivPages>
      <PageButtonPrevious
        onClick={() => {
          previousPage();
        }}
      >
        <ArrowBackIcon />
      </PageButtonPrevious>
      <PageButtonNext
        onClick={() => {
          nextPage();
        }}
      >
        <ArrowForwardIcon />
      </PageButtonNext>
    </DivPages>
  );
};

export default ChangePage;
