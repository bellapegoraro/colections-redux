import "./App.css";
import HomeImages from "./components/HomeImages/index";
import Header from "./components/header/index";
import { useLocation } from "react-router-dom";
import Routes from "./components/Routes/index";
import ChangePage from "./components/ChangePage/index";

const App = () => {
  const location = useLocation();

  return (
    <>
      <Header />
      {location.pathname === "/" ? <HomeImages /> : null}
      {location.pathname === "/" ? null : (
        <div className="changePage">
          <ChangePage />
        </div>
      )}
      <div className="main">
        <Routes />
      </div>
    </>
  );
};

export default App;
